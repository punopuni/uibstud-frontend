$(document).ready(function () {
    /**
     * showTabs - функция для показа табоы
     * defaultOpen - таб по умолчагию открытый
     */
    if (document.getElementsByClassName("tab-pane").length > 0) {
        function showTabs(event) {
            var name = event.target.getAttribute('data-target');

            var tabContent = document.getElementsByClassName("tab-pane");

            var i, tabLinks;

            for (i = 0; i < tabContent.length; i++) {
                tabContent[i].style.display = "none";
            }
            tabLinks = document.getElementsByClassName("nav-link");
            var thisItem = this;

            for (i = 0; i < tabLinks.length; i++) {
                tabLinks[i].className = tabLinks[i].className.replace(" active", "");
            }
            event.currentTarget.className += " active";
            document.getElementById(name).style.display = "block";
        }

        var defaultOpen = document.getElementById("defaultOpen").getAttribute('data-target');
        document.getElementById(defaultOpen).style.display = "block";
        document.getElementById("defaultOpen").className += " active";
        var navs = document.getElementsByClassName('nav-link');
        for (var i = 0; i < navs.length; i++) {
            navs[i].addEventListener('click', showTabs, false)
        }
    }

});