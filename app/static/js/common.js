$(document).ready(function () {
    $(".header__avatar").click(function () {
        $(this).find('.header__exit').fadeTo("slow", 1).css("z-index", "10");
    });
    $(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".header__avatar"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			div.find('.header__exit').fadeTo("slow", 0).css("z-index", "-1");; // скрываем его
		}
	});
});